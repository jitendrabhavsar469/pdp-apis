<?php
/*
Plugin Name: PDP APIs
*/

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Main plugin class
 */
class PspAPIs
{
    /**
     * Initialize the plugin
     */
    public function __construct()
    {
        // Plugin initialization actions
        add_action('init', array($this, 'init'));
    }

    /**
     * Initialize the plugin
     */
    public function init()
    {
        // Register the custom API route
        add_action('rest_api_init', array($this, 'register_product_details_api_route'));
    }

    /**
     * Register WooCommerce product details API route
     */
    public function register_product_details_api_route()
    {
        register_rest_route('vendco/v1', '/product-details', array(
            'methods' => 'GET',
            'callback' => array($this, 'get_product_details'),
        ));
    }

    /**
 * Callback function to handle the API request
 *
 * @param WP_REST_Request $request The request object.
 * @return WP_REST_Response
 */
public function get_product_details($request)
{
    // Retrieve request parameters
    $product_id = $request->get_param('id');
    $sort_by = $request->get_param('sort_by');
    $sort = $request->get_param('sort');
    $page = $request->get_param('page');
    $per_page = $request->get_param('per_page');
    $paginate = $request->get_param('paginate');
    $category = $request->get_param('category');

    // Check if product ID is provided and it is a valid integer
    if ($product_id && !is_numeric($product_id)) {
        // Invalid product ID provided, create an error response
        $response = new WP_REST_Response(array(
            'success' => false,
            'message' => 'Invalid product ID provided.',
        ));
        $response->set_status(400); // Bad Request

        return $response;
    }

    // Check if sort parameter is provided and it has a valid value
    if ($sort && !in_array(strtolower($sort), array('asc', 'desc'))) {
        // Invalid sort value provided, create an error response
        $response = new WP_REST_Response(array(
            'success' => false,
            'message' => 'Invalid sort value provided. Allowed values are "asc" or "desc".',
        ));
        $response->set_status(400); // Bad Request

        return $response;
    }

    // Check if Advanced Custom Fields (ACF) is enabled
    $acf_enabled = function_exists('get_field_objects');

    // Set default values for pagination
    $default_page = 1;
    $default_per_page = 10;

    // Validate and sanitize the page parameter
    $page = $page ? max(1, absint($page)) : $default_page;

    // Validate and sanitize the per_page parameter
    $per_page = $per_page ? max(1, absint($per_page)) : $default_per_page;

    // Include ACF fields if enabled
    $acf_fields = array();
    if ($acf_enabled) {
        // Retrieve ACF fields for the "product" post type
        $acf_fields = $this->get_all_acf_fields('product');
    }

    // Create an empty array to store the queried product IDs
    $product_ids = array();

    // Check if category parameter is provided
    if ($category) {
        // Retrieve the product IDs for the given category
        $category_term = get_term_by('slug', $category, 'product_cat');
        if ($category_term) {
            $product_args = array(
                'post_type' => 'product',
                'post_status' => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'term_id',
                        'terms' => $category_term->term_id,
                    ),
                ),
                'fields' => 'ids',
            );

            $product_ids = get_posts($product_args);
        }
    }

    // Check if product ID is provided
    if ($product_id) {
        // If category filtering is enabled, check if the product belongs to the specified category
        if ($category && !in_array($product_id, $product_ids)) {
            // Product does not belong to the specified category, create an error response
            $response = new WP_REST_Response(array(
                'success' => false,
                'message' => 'Product not found in the specified category.',
            ));
            $response->set_status(404);

            return $response;
        }

        // Retrieve the product details based on the given product ID
        $product = wc_get_product($product_id);

        // Check if the product exists
        if ($product) {
            // Prepare the response data
            $data = $this->prepare_product_data($product, $acf_fields);

            // Create a success response
            $response = new WP_REST_Response(array(
                'success' => true,
                'data' => $data,
            ));
        } else {
            // Product not found, create an error response
            $response = new WP_REST_Response(array(
                'success' => false,
                'message' => 'Product not found.',
            ));
            $response->set_status(404);
        }
    } else {
        // Get all products
        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
        );

        // If category filtering is enabled, filter the products by the specified category
        if ($category) {
            $args['post__in'] = $product_ids;
        }

        // Check if the sort parameter is provided and valid
        if ($sort && in_array(strtolower($sort), array('asc', 'desc'))) {
            $args['orderby'] = 'date';
            $args['order'] = strtolower($sort) === 'asc' ? 'ASC' : 'DESC';
        }

        // Check if the sort_by parameter is provided and valid
        if ($sort_by) {
            switch ($sort_by) {
                case 'id':
                    $args['orderby'] = 'ID';
                    break;
                case 'title':
                    $args['orderby'] = 'title';
                    break;
                case 'price':
                    $args['orderby'] = 'meta_value_num';
                    $args['meta_key'] = '_price';
                    break;
                case 'date_created':
                    $args['orderby'] = 'date';
                    break;
                default:
                    // Invalid sort_by value provided, create an error response
                    $response = new WP_REST_Response(array(
                        'success' => false,
                        'message' => 'Invalid sort_by value provided. Allowed values are "id", "title", "price", "date_created".',
                    ));
                    $response->set_status(400); // Bad Request

                    return $response;
            }
        }

        // Check if pagination is enabled
        if ($paginate && $paginate === 'true') {
            // Calculate the total count of products
            $query_count = new WP_Query($args);
            $total_count = $query_count->found_posts;
            $max_page = ceil($total_count / $per_page);

            // Check if the current page exceeds the maximum page
            if ($page > $max_page) {
                $response = new WP_REST_Response(array(
                    'success' => false,
                    'message' => 'Page number exceeds maximum page.',
                ));
                $response->set_status(400); // Bad Request

                return $response;
            }

            // Calculate the offset based on the current page and per_page value
            $offset = ($page - 1) * $per_page;

            // Set the offset and per_page values for pagination
            $args['offset'] = $offset;
            $args['posts_per_page'] = $per_page;
        }

        // Retrieve the products based on the arguments
        $query = new WP_Query($args);
        $products = $query->get_posts();

        // Prepare the response data
        $data = array();
        foreach ($products as $product) {
            $product_obj = wc_get_product($product);
            $product_data = $this->prepare_product_data($product_obj, $acf_fields);

            $data[] = $product_data;
        }

        // Calculate pagination meta data if pagination is enabled
        $pagination_meta = null;
        if ($paginate && $paginate === 'true') {
            $total_pages = $query->max_num_pages;

            $pagination_meta = array(
                'total_count' => $total_count,
                'total_pages' => $total_pages,
                'current_page' => $page,
                'per_page' => $per_page,
            );
        }

        // Create a success response
        $response = new WP_REST_Response(array(
            'success' => true,
            'data' => $data,
            'pagination' => $pagination_meta,
        ));
    }

    return $response;
}


    /**
     * Prepare the product data for response.
     *
     * @param WC_Product $product The WooCommerce product object.
     * @param array $acf_fields The ACF fields.
     * @return array The prepared product data.
     */
    private function prepare_product_data($product, $acf_fields)
    {
        // Typecast the price to integer
        $price = (int) $product->get_price();

        $product_data = array(
            'id' => $product->get_id(),
            'name' => $product->get_name(),
            'description' => $product->get_description(),
            'price' => $price,
            'created_date' => $product->get_date_created()->date('Y-m-d H:i:s'),
            'product_url' => get_permalink($product->get_id()),
            'image_url' => $this->get_product_image_url($product),
            'categories' => wp_get_post_terms($product->get_id(), 'product_cat', array('fields' => 'names')),
            'type' => $product->get_type(),
            // Add any other product details you want to include
        );

        // Include grouped product details
        if ($product->is_type('grouped')) {
            $grouped_products = array();

            foreach ($product->get_children() as $child_id) {
                $child_product = wc_get_product($child_id);

                $child_product_data = array(
                    'id' => $child_product->get_id(),
                    'name' => $child_product->get_name(),
                    'description' => $child_product->get_description(),
                    'price' => (int) $child_product->get_price(),
                    'created_date' => $child_product->get_date_created()->date('Y-m-d H:i:s'),
                    'product_url' => get_permalink($child_product->get_id()),
                    'image_url' => $this->get_product_image_url($child_product),
                    'categories' => wp_get_post_terms($child_product->get_id(), 'product_cat', array('fields' => 'names')),
                    'type' => $child_product->get_type(),
                    // Add any other child product details you want to include
                );

                $grouped_products[] = $child_product_data;
            }

            $product_data['grouped_products'] = $grouped_products;
        }

        // Include variation details if the product is a variable product
        if ($product->is_type('variable')) {
            $variations = array();

            foreach ($product->get_available_variations() as $variation) {
                $variation_id = $variation['variation_id'];
                $variation_product = wc_get_product($variation_id);

                $variation_data = array(
                    'id' => $variation_id,
                    'attributes' => $variation_product->get_variation_attributes(),
                    'price' => (int) $variation_product->get_price(),
                    'image_url' => $this->get_product_image_url($variation_product),
                );

                $variations[] = $variation_data;
            }

            $product_data['variations'] = $variations;
        }

        // Include ACF fields if enabled
        if ($acf_fields) {
            $product_acf_fields = $this->group_acf_fields($acf_fields, $product->get_id());
            $product_data['acf_fields'] = $product_acf_fields;
        }

        // Include external/affiliate product details
        if ($product->is_type('external')) {
            $product_data['external_url'] = $product->get_product_url();
            $product_data['button_text'] = $product->get_button_text();
        }

        return $product_data;
    }

    /**
     * Get the image URL of the product.
     *
     * @param WC_Product $product The WooCommerce product object.
     * @return string The image URL.
     */
    private function get_product_image_url($product)
    {
        if ($product->is_type('variable') && $product->get_image_id() === 0) {
            // For variable products, use the first variation image as the product image
            $variations = $product->get_available_variations();
            if (!empty($variations[0]['image_id'])) {
                return wp_get_attachment_url($variations[0]['image_id']);
            }
        } elseif ($product->get_image_id()) {
            return wp_get_attachment_url($product->get_image_id());
        }

        return '';
    }

    /**
     * Group ACF fields by field group names.
     *
     * @param array $acf_fields ACF field groups and their fields.
     * @param int $post_id The ID of the post for which to retrieve ACF field values.
     * @return array An array of ACF fields grouped by field group names.
     */
    private function group_acf_fields($acf_fields, $post_id)
    {
        $grouped_fields = array();

        foreach ($acf_fields as $acf_group) {
            $field_group = $acf_group['field_group'];
            $fields = $acf_group['fields'];

            $group_name = $field_group['title'];

            foreach ($fields as $field) {
                // Retrieve the field value for the specific post ID
                $field_value = get_field($field['key'], $post_id);

                // Check if the field is a parent field type group (e.g., repeater or group)
                if ($this->is_parent_field_group($field)) {
                    // Group the child fields within the parent field
                    $child_fields = array();
                    foreach ($field['sub_fields'] as $child_field) {
                        $child_field_value = $this->get_field_value($child_field, $field_value);
                        $child_fields[$child_field['name']] = $child_field_value;
                    }

                    $field_value = $child_fields;
                } elseif ($field['type'] === 'image') {
                    // Handle image fields to return only the URL
                    if (is_numeric($field_value)) {
                        $image_url = wp_get_attachment_url($field_value);
                        $field_value = $image_url ? $image_url : '';
                    } elseif (is_array($field_value)) {
                        if (!empty($field_value['url'])) {
                            $field_value = $field_value['url'];
                        } elseif (!empty($field_value['ID'])) {
                            $image_url = wp_get_attachment_url($field_value['ID']);
                            $field_value = $image_url ? $image_url : '';
                        } else {
                            $field_value = '';
                        }
                    } else {
                        $field_value = '';
                    }
                }

                // Add the field key-value pair to the grouped fields
                $grouped_fields[$group_name][$field['name']] = $field_value;
            }
        }

        return $grouped_fields;
    }

    /**
     * Get the value of an ACF field.
     *
     * @param array $field The ACF field.
     * @param mixed $parent_value The parent field value.
     * @return mixed The field value.
     */
    private function get_field_value($field, $parent_value)
    {
        // Retrieve the field value from the parent field
        $field_value = isset($parent_value[$field['name']]) ? $parent_value[$field['name']] : '';

        // Handle image fields to return only the URL
        if ($field['type'] === 'image' && is_numeric($field_value)) {
            $image_url = wp_get_attachment_url($field_value);
            $field_value = $image_url ? $image_url : '';
        }

        return $field_value;
    }

    /**
     * Check if a field is a parent field type group (e.g., repeater or group).
     *
     * @param array $field The ACF field.
     * @return bool Whether the field is a parent field type group.
     */
    private function is_parent_field_group($field)
    {
        $field_type = $field['type'];
        return in_array($field_type, array('repeater', 'flexible_content', 'group'), true);
    }

    /**
     * Get all ACF fields of a specific post type.
     *
     * @param string $post_type The post type to retrieve ACF fields for.
     * @return array An array of ACF field groups and their fields.
     */
    private function get_all_acf_fields($post_type)
    {
        $acf_fields = array();
        $groups = acf_get_field_groups(array('post_type' => $post_type));

        foreach ($groups as $group) {
            $acf_fields[] = array(
                'field_group' => $group,
                'fields' => acf_get_fields($group['key']),
            );
        }

        return $acf_fields;
    }

    /**
     * Plugin activation hook
     */
    public static function activate()
    {
        // Add activation code here
    }

    /**
     * Plugin deactivation hook
     */
    public static function deactivate()
    {
        // Add deactivation code here
    }
}

// Instantiate the plugin class
$psp_apis_plugin = new PspAPIs();

// Plugin activation hook
register_activation_hook(__FILE__, array('PspAPIs', 'activate'));

// Plugin deactivation hook
register_deactivation_hook(__FILE__, array('PspAPIs', 'deactivate'));
