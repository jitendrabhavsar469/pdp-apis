// A simple product listing api

endpoint : siteurl/wp-json/vendco/v1/product-details

parameters-accepted : id(int) -- product id to get details of specific product
                    sort(asc|dsc) --  to sort retrived results ascending or descending
                     paginate(bool:true/false) -- (default false) choose if results should be in pagination format or not
                     page -- page number for pagination if paginate true
                     per_page -- result to display per page if paginate true
                     category -- to filter products based on catagory
                     sort_by -- sorting parmeter based on which sorting should be done [id,price,title,date_created]
